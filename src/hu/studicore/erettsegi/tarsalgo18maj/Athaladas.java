package hu.studicore.erettsegi.tarsalgo18maj;

/**
 *
 * @author Pasztuhov Dániel, StudiCore Kft.
 */
public class Athaladas {
    public int ora;
    public int perc;
    public int azonosito;
    // az irany lehetne még esetleg boolean is, vagy int.
    public String irany;
}
