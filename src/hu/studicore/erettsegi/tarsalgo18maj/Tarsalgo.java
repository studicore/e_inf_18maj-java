package hu.studicore.erettsegi.tarsalgo18maj;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pasztuhov Dániel, StudiCore Kft.
 */
public class Tarsalgo {

    public static String idopontString(int ora, int perc) {
        String ret = "";
        if (ora < 10)
            ret += "0";
        ret += ora;
        ret += ":";
        if (perc < 10)
            ret += "0";
        ret += perc;
        return ret;
    }
    
    public static void main(String[] args) {
        // Ha követted a tanácsaimat, akkor ezt a részt betanultad.
        // Az alábbi megoldás az adatok elemenkénti beolvasásával működik.
        // Lehet úgy is, hogy beolvassuk a teljes sort, spliteljük, majd azt alakítjuk át úgy, hogy el tudjuk tárolni.
        // Python esetében is konvertáljuk lehetőleg egész számmá, amit lehet!
        List<Athaladas> lista = new ArrayList<>();
        try {
            Scanner fajl = new Scanner(new File("ajto.txt"));
            while (fajl.hasNextInt()) {
                Athaladas athaladas = new Athaladas();
                athaladas.ora = fajl.nextInt();
                athaladas.perc = fajl.nextInt();
                athaladas.azonosito = fajl.nextInt();
                athaladas.irany = fajl.nextLine().trim(); // trim: hogy az elválasztó szóköz ne maradjon benne
                lista.add(athaladas);
            }
            fajl.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Tarsalgo.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("2. feladat");
        System.out.println("Az első belépő: "+lista.get(0).azonosito);
        // keresés/kiválasztás hátulról előre felé:
        for (int i = lista.size()-1; i >= 0; i--) {
            if (lista.get(i).irany.equals("ki")) {
                // megtaláltuk az utolsó kilépési adatot
                System.out.println("Az utolsó kilépő: " + lista.get(i).azonosito);
                break; // vagy break helyett úgy állítod össze a feltételt, hogy kilépjen, ha megvan az elem.
            }
        }
        // 3. feladat
        int[] athaladasok = new int[101]; // hogy legyen 1-100-ig index
        for (Athaladas a : lista) { // végigmegyünk az összes elemen, minden elem bekerül a-ba, 
                                    // majd végrehajtódik rá a ciklus törzse.
            athaladasok[a.azonosito]++; // a.azonosito a színész azonosítója 1-100,
                // ami pont jó nekünk indexnek: az athaladasok tömb azon elemét növeljük.
        }
        try {
            PrintWriter pw = new PrintWriter("athaladas.txt");
            for (int i = 1; i <= 100; i++) {
				// "Határozza meg, hogy a fájlban szereplő személyek közül..."
				// tehát ki kell válogatnunk azokat, akik áthaladtak - azaz benne voltak a fájlban.
                if (athaladasok[i] != 0)
					pw.println(i + " " + athaladasok[i]);
            }
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Tarsalgo.class.getName()).log(Level.SEVERE, null, ex);
        }
        // 4. és 5. feladat: szimuláljuk a ki- és belépéseket:
        boolean[] bentvan = new boolean[101];
        int osszbent = 0;
        int max = 0; // választhatjuk a maximum kezdeti értékének a 0-t, mert kezdetben ennyien voltak bent (első elem feldolgozása).
        int maxora = 8, maxperc = 59;
        for (Athaladas a : lista) {
            // az irány vagy be, vagy ki, nincs harmadik lehetőség.
            // És kétszer senki nem tud bejönni, amíg ki nem megy.
            if (a.irany.equals("be")) {
                bentvan[a.azonosito] = true;
                osszbent++;
            } else {
                bentvan[a.azonosito] = false;
                osszbent--;
            }
            // 5. feladat miatt az osszbent változóban lévő számokra futtatunk egy maximumkiválasztást.
            if (osszbent > max) {
                max = osszbent;
                // kell az óra:perc is, viszont tetszőleges maximum időpontot eltárolhatjuk: az elsőt tároljuk el.
                maxora = a.ora;
                maxperc = a.perc;
            }
        }
        System.out.println("4. feladat");
        for (int i = 1; i <= 100; i++) {
            if (bentvan[i]) {
                System.out.print(i+" ");
            }
        }
        System.out.println();
        System.out.println("5. feladat");
        // formázatlan kimenet, pl. 10:0
        //System.out.println("Például "+maxora+":"+maxperc+"-kor voltak a legtöbben a társalgóban.");
        // formázott kimenet:
        System.out.printf("Például %02d:%02d-kor voltak a legtöbben a társalgóban.%n", maxora, maxperc);
        // vagy készíthetsz sajt időpontkiíró függvényt:
        //System.out.println("Például "+idopontString(maxora, maxperc)+"-kor voltak a legtöbben a társalgóban.");
        System.out.println("6. feladat");
        Scanner sc = new Scanner(System.in);
        System.out.print("Adja meg a személy azonosítóját! ");
        int keresett = sc.nextInt();
        System.out.println("7. feladat");
        int osszesPerc = 0;
        int utolsoBePercben = 0;
        for (Athaladas a : lista) {
            if (a.azonosito == keresett) {
                // Biztosan először minden személy esetében belépés jön, és csak
                // utána jön a kilépése, hiszen kezdetben üres volt a társalgó.
                if (a.irany.equals("be")) {
                    //System.out.print(a.ora+":"+a.perc+"-"); // nem formázott
                    System.out.printf("%02d:%02d-", a.ora, a.perc);
                    //System.out.print(idopontString(a.ora, a.perc)+"-");
                    utolsoBePercben = a.ora*60+a.perc;
                } else { // ha nem belépés, akkor kilépés
                    //System.out.println(a.ora+":"+a.perc); // nem formázott
                    System.out.printf("%02d:%02d%n", a.ora, a.perc);
                    //System.out.println(idopontString(a.ora, a.perc));
                    int mostPerc = a.ora*60+a.perc;
                    osszesPerc += (mostPerc - utolsoBePercben);
                }
            }
        }
        // Ha vége van a megfigyelésnek, és a személy a társalgóban van még,
        // akkor 15:00-ig bent töltött perceket hozzá kell még adni:
        if (bentvan[keresett]) {
            System.out.println(); // hogy szépen írja ki
            int mostPerc = 15*60;
            osszesPerc += (mostPerc - utolsoBePercben);
        }
        System.out.print("A(z) "+keresett+". személy összesen "+osszesPerc+" percet volt bent, a megfigyelés végén ");
        if (bentvan[keresett]) {
            System.out.println("a társalgóban volt.");
        } else {
            System.out.println("nem volt a társalgóban.");
        }
    }
    
}
