Emelt szintű informatika érettségi, nem hivatalos megoldás (2018. május 14.)
====

Pasztuhov Dániel vagyok, a StudiCore Kft. vezetője, illetve évek óta programozásoktató. Az elmúlt években többször volt alkalmam emelt szintű érettségi 4. feladatára felkészíteni diákokat. Ebben az évben is két tanulómért izgulhattunk.

Tegnap megvolt az emelt szintű informatika érettségi gyakorlati része.

Az egyik, a nyári egyetemi előkészítő programozástanfolyamunk iránt érdeklődő most érettségiző tanuló kérésére, javaslatára összefoglaltam, hogy hogyan is oldanám meg a 4. feladat részfeladatait.

Először végigolvasnám a feladatokat, ahogyan az [érettségire adott jó tanácsaim](https://www.studicore.hu/erettsegi_tanacsok_2018.pdf) dokumentumában szerepel is. Vastaggal kiemelve a szöveg olvasása kapcsán eszembe jutó gondolataim.

A feladat szövegének elolvasása
---

**Társalgó**

Egy színház társalgójában még a délelőtti próbák alatt is nagy a forgalom. A színészek hosszabb-rövidebb beszélgetésekre térnek be ide, vagy éppen csak keresnek valakit. A feladatban a társalgó ajtajánál 9 és 15 óra között felvett adatokat kell feldolgoznia. **(Vajon csak ilyen adatok lesznek, vagy ki is kell majd válogatni a megfelelőeket?)**

Az `ajto.txt` fájlban időrendben rögzítették, hogy ki és mikor lépett be vagy ki a társalgó egyetlen ajtaján. **(Legalább nem kell több ajtóval foglalkozni)** A fájl soraiban négy, szóközzel elválasztott érték található. **(Alighanem splitelni fogunk – vagy elemenként olvassuk be a fájlt)** Az első két szám az áthaladás időpontja (óra, perc), a harmadik a személy azonosítója, az utolsó az áthaladás iránya (be/ki). A sorok száma legfeljebb 1000, a személyek azonosítója egy 1 és 100 közötti egész szám. Biztosan tudjuk, hogy a megfigyelés kezdetén (9 órakor) a társalgó üres volt, de a megfigyelés végén (15 órakor) még lehettek benn a társalgóban. **(Mégsem kell szűrni, csak 9-15 óra közötti adatok szerepelnek)** A társalgóba be- és kilépéseket azok sorrendjében tartalmazza az állomány, még akkor is, ha a perc pontossággal rögzített adatok alapján egyezőség áll fenn. **(Idő szerint rendezett állomány)**.

**A legtöbb esetben valamilyen összetartozó adatokból álló elemek listája az adatszerkezet. Első olvasásra feltételezem, hogy ezt használom, aztán a végén megnézem, hogy tényleg jó volt-e végig.**

Például:
![bemeneti fájl példa](https://www.studicore.hu/e_inf_18maj_pelda.png "Bemeneti fájl példa")

A fenti példában a szürke mintázatú részen a bemeneti fájl első néhány sora látható. A második sora azt mutatja, hogy a 9-es azonosítójú személy 9 óra 1 perckor lépett be a társalgóba. A negyedik sorban olvasható, hogy 9 óra 5 perckor már ki is ment, tehát ekkor összesen 4 percet töltött bent. A szürke rész sorai mellett olvasható számok azt mutatják, hogy a be- vagy kilépést követően hányan vannak bent a társalgóban. Ez a szám egy percen belül akár többször is változhat.

Készítsen programot, amely az `ajto.txt` állomány adatait felhasználva az alábbi kérdésekre válaszol! A program forráskódját mentse tarsalgo néven! (A program megírásakor a felhasználó által megadott adatok helyességét, érvényességét nem kell ellenőriznie, feltételezheti, hogy a rendelkezésre álló adatok a leírtaknak megfelelnek.)

A képernyőre írást igénylő részfeladatok eredményének megjelenítése előtt írja a képernyőre a feladat sorszámát (például: 4. feladat:)! Ha a felhasználótól kér be adatot, jelenítse meg a képernyőn, hogy milyen értéket vár! Az ékezetmentes kiírás is elfogadott.

1.   Olvassa be és tárolja el az `ajto.txt` fájl tartalmát! **(Általában ezzel szokott kezdődni)**
2.   Írja a képernyőre annak a személynek az azonosítóját, aki a vizsgált időszakon belül először lépett be az ajtón, és azét, aki utoljára távozott a megfigyelési időszakban! **(legelőször érkező: az első beolvasott sor, mivel a feladat is írja, hogy kezdetben a társalgó üres volt – ha ez nem állna fenn, akkor az első „be”-vel jelölt bejegyzésben van az első belépő; utoljára távozó: mivel 15 óra után nem zár be a társalgó, maradhatnak benn emberek, hátulról az első olyan bejegyzés, ami-ben „ki” szerepel – ez lineáris keresés / kiválasztás segítségével kezelhető)**.
3.   Határozza meg a fájlban szereplő személyek közül, ki hányszor haladt át a társalgó ajtaján! **(ha egy színész adataira lennénk kíváncsiak, akkor azt lehetne egy darab megszámlálással kezelni, de most az összes színészre kíváncsiak vagyunk. A feladat korábbi része szerint a színészek azonosítója 1-100 között lehet, és sorban kell kiírni. Ilyenkor praktikus választás egy 100(+1) elemű tömb/lista: a Python, a C# és a Java is 0-val kezd indexelni – nekünk praktikusabb, ha a színész azonosítója lesz az index, a 0. indexű elemet meg nem használjuk… ebbe a tömbbe fogjuk tenni a 100 darab színésszel kapcsolatos megszámlálások eredményét… Ha a színészeket nem 1-100-ig terjedő számokkal azonosítanánk, akkor még jól jöhetne a Map/Dictionary is)**
A meghatározott értékeket azonosító szerint növekvő sorrendben írja az `athaladas.txt` fájlba! **(Így a tömb tényleg praktikus, mert azonosító szerint van sorban)** Soronként egy személy azonosítója, és tőle egy szóközzel elválasztva az áthaladások száma szerepeljen!
4.   Írja a képernyőre azon személyek azonosítóját, akik a vizsgált időszak végén a társalgóban tartózkodtak!
**Jó ötlet lenne esetleg szimulálni a ki-belépést? Lehetne egy tömbünk/listánk arról, hogy az adott azonosítójú színész bent tartózkodik-e vagy sem.**
5.   Hányan voltak legtöbben egyszerre a társalgóban? Írjon a képernyőre egy olyan időpontot
(óra:perc), amikor a legtöbben voltak bent!
**... ha az előző feladatban a szimulálást választanánk, akkor menet közben számolhatnánk azt is, hogy épp hányan vannak bent, illetve futhatna egy maximumkiválasztás is rá.**
6.   Kérje be a felhasználótól egy személy azonosítóját! A további feladatok megoldásánál ezt használja fel!
Feltételezheti, hogy a megadott azonosítóhoz tartozik adat a forrásfájlban.
Bekérés.
7.   Írja a képernyőre, hogy a beolvasott azonosítóhoz tartozó személy mettől meddig tartózkodott a társalgóban!
A kiírást az alábbi, 22-es személyhez tartozó példának megfelelően alakítsa ki!
**Végigolvassuk a listánkat, kiválogatjuk az adott személyhez tartozó adatokat, és ezen belül „be” esetén kiírjuk a sor első felét, „ki” esetén befejezzük a második felével.**

    ```
    11:22-11:27
    13:45-13:47
    13:53-13:58
    14:17-14:20
    14:57-
    ```

8.  Határozza meg, hogy a megfigyelt időszakban a beolvasott azonosítójú személy összesen hány percet töltött a társalgóban! **(Ha az előző feladatban végignéztük az adott személy ki és belépéseit, akkor már a bent töltött perceket is össze tudjuk számolni)** Az előző feladatban példaként szereplő 22-es személy
5 alkalommal járt bent, a megfigyelés végén még bent volt. Róla azt tudjuk, hogy 18 percet töltött bent a megfigyelés végéig. A 39-es személy 6 alkalommal járt bent, a vizsgált időszak végén nem tartózkodott a helyiségben. Róla azt tudjuk, hogy 39 percet töltött ott. Írja ki, hogy a beolvasott azonosítójú személy mennyi időt volt a társalgóban, és a megfigyelési időszak végén bent volt-e még!

Adatszerkezet
---

**Az utolsó feladat is könnyedén megoldható a választott be- és kilépési adatok listájával, nem kell rajta módosítani.
Az összetartozó adatokat egy külön Java osztályba fogom tenni (C# struct/class, Python tuple/dictionary használható). Az objektum-orientált programozás nem szükséges készség egy érettségi jó megírásához, ezért most Javában is struct-szerű osztályt használok: minden adattag nyilvános lesz.**

```
public class Athaladas {
    public int ora;
    public int perc;
    public int azonosito;
    public String irany;
}
```

Megoldás
---
További megjegyzések a program forráskódjában:
[Emelt szintű informatika érettségi 4. feladatának megoldása, 2018. május (forráskód)](https://gitlab.com/studicore/e_inf_18maj-java)

Nyári egyetemi felkészítő
---

2018 nyarán szervezünk egy-egy nyári, egyetemi felkészítő tanfolyamot a BME és az SZTE programozó szakjaira készülők számára.

Sokaknak azért van gondjuk az egyetemen, mert a programozás nem ment nekik: középiskolában nem volt elég nekik a hivatalos oktatás, önállóan pedig nem képezték magukat.

Azt is érdemes tudni, hogy ha az emelt szintű érettségit lazán megírod, az még általában nem elegendő ahhoz, hogy első félévben ne legyenek gondjaid a programozással.

Azért szervezzük ezeket a tanfolyamokat, hogy segítsünk neked abban, hogy könnyedén vedd programozásból az első félévet: megtanítunk rendesen programozni. Mindehhez egy-egy olyan oktatót választottunk, aki az adott egyetemen bennfentes, és ismeri, mire van szüksége egy frissen kezdő tanulónak. Mindezt laza, oldott hangulatban, a leendő évfolyamtársaid körében.

SZTE: [https://www.studicore.hu/szte](https://www.studicore.hu/erettsegi-utan-irany-az-szte-prog?utm_source=erettseggitlab&utm_medium=gitlab&utm_campaign=erettsegimo20180515)

BME: [https://www.studicore.hu/bme](https://www.studicore.hu/erettsegi-utan-irany-a-bme-mernokinfo?utm_source=erettseggitlab&utm_medium=gitlab&utm_campaign=erettsegimo20180515)

Ha pedig nem 2018-ban nézed ezt a dokumentumot, akkor pedig keress meg bennünket a friss információkért: info@studicore.hu

*Pasztuhov Dániel*
